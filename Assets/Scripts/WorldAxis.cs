﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldAxis : MonoBehaviour
{
    public float Size = 1f;

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(Vector3.right * Size, Vector3.zero);

        Gizmos.color = Color.green;
        Gizmos.DrawLine(Vector3.up * Size, Vector3.zero);

        Gizmos.color = Color.blue;
        Gizmos.DrawLine(Vector3.forward * Size, Vector3.zero);
        Gizmos.color = Color.white;
    }
}
