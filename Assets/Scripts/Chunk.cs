﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    public struct ChunkIndex
    {
        public int X;
        public int Z;

        public ChunkIndex(int x, int z)
        {
            this.X = x;
            Z = z;
        }
        
        public static explicit operator Vector3(ChunkIndex c) => new Vector3(c.X, 0f, c.Z);
        public static explicit operator Vector2(ChunkIndex c) => new Vector2(c.X, c.Z);
        public static explicit operator ChunkIndex(Vector3 vec) => new ChunkIndex((int) vec.x, (int) vec.z);

        public static bool operator ==(ChunkIndex index, ChunkIndex other)
        {
            return index.Equals(other);
        }

        public static bool operator !=(ChunkIndex index, ChunkIndex other)
        {
            return !(index == other);
        }

        public bool Equals(ChunkIndex other)
        {
            return X == other.X && Z == other.Z;
        }

        public override bool Equals(object obj)
        {
            return obj is ChunkIndex other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (X * 397) ^ Z;
            }
        }
    }
    
    class Chunk
    {
        private const int MountainHeight = 100;
        private const int BaseTerrainHeight = 40;
        
        public const int Height = 150;
        public const int Width = 64;
        /// <summary>
        /// Ordered after x, z, y
        /// </summary>
        public BlockData[,,] Blocks = new BlockData[Width, Width, Height];


        public static Chunk Generate(float perlinX, float perlinY, float perlinXStep, float perlinYStep)
        {
            Chunk chunk = new Chunk();

            for (int x = 0; x < Width; x++)
            {
                for (int z = 0; z < Width; z++)
                {
                    float noise = Mathf.PerlinNoise(perlinX + perlinXStep * x, perlinY + perlinYStep * z);
                    
                    int height = (int) Mathf.Round(BaseTerrainHeight + noise * (Height - BaseTerrainHeight));
                    for (int y = 0; y < height; y++)
                    {
                        if (y == height - 1)
                        {
                            if (y > MountainHeight - Random.Range(0, 5))
                            {
                                chunk.Blocks[x, z, y] = new BlockData {BlockType = BlockType.Snowy};
                            }
                            else
                            {
                                chunk.Blocks[x, z, y] = new BlockData {BlockType = BlockType.Grass};
                            }
                        }
                        else
                        {
                            if (height - y > 3 + Random.Range(0, 5))
                            {
                                chunk.Blocks[x, z, y] = new BlockData {BlockType = BlockType.Stone};
                            }
                            else
                            {
                                chunk.Blocks[x, z, y] = new BlockData {BlockType = BlockType.Dirt};
                            }
                        }
                        
                    }
                }
            }

            return chunk;
        }
        
        public static ChunkIndex WorldToChunkIndex(Vector3 val)
        {
            return new ChunkIndex( Mathf.FloorToInt(val.x / Chunk.Width), Mathf.FloorToInt(val.z / Chunk.Width));
        }
        
        public static Vector3 ChunkIndexToWorld(ChunkIndex val)
        {
            return new Vector3(val.X * Chunk.Width, 0f, val.Z * Chunk.Width);
        }
    }
}
