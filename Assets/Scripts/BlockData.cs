﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction : byte
{
    West, North, East, South, Bottom, Top
}

public enum BlockType : byte
{
    Air, Dirt, Grass, Stone, Snowy
}

public struct BlockData
{
    public BlockType BlockType;
}
