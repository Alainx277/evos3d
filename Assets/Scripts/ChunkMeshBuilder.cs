﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Rendering;

namespace Assets.Scripts
{
    class ChunkMeshBuilder
    {
        private readonly Chunk _chunk;
        private readonly BlockAtlas _atlas;
        private readonly Chunk[] _adjacent;

        public ChunkMeshBuilder(Chunk chunk, BlockAtlas atlas)
        {
            _chunk = chunk;
            _atlas = atlas;
            _adjacent = new Chunk[4];
        }

        public void AddAdjacent(Direction direction, Chunk chunk)
        {
            _adjacent[(int) direction] = chunk;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Chunk GetAdjacent(Direction direction)
        {
            return _adjacent[(int) direction];
        }

        public Mesh Build()
        {
            BlockData[,,] blocks = _chunk.Blocks;
            int height = Chunk.Height;
            int width = Chunk.Width;

            // Allocate maximum required memory to prevent reallocation
            // TODO: Find realistic percentages
            int maximumBlocks = Chunk.Height* Chunk.Width* Chunk.Width;
            var vertices = new List<Vector3>(maximumBlocks * 8);
            var uvs = new Vector2[maximumBlocks * 8];
            var triangles = new List<int>(maximumBlocks * 6 * 2 * 3);

            for (int x = 0; x < width; x++)
            {
                for (int z = 0; z < width; z++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        // Ignore air blocks
                        if (blocks[x, z, y].BlockType == BlockType.Air)
                        {
                            continue;
                        }
                        
                        BlockFace[] blockFaces = _atlas.GetBlockFaces(blocks[x, z, y].BlockType);

                        // Bottom face
                        if (/*y == 0 || don't render bottom of world*/ y != 0 && blocks[x, z, y - 1].BlockType == BlockType.Air)
                        {
                            var face = AddFaceVertices(x, y, z, vertices, Direction.Bottom);
                            (int bottomLeft, int bottomRight, int topRight, int topLeft) = face;
                            BuildQuad(triangles, face);
                            BlockFace uv = blockFaces[(int) Direction.Bottom];
                            uvs[bottomLeft] = uv.LowerLeft;
                            uvs[bottomRight] = uv.LowerRight;
                            uvs[topRight] = uv.UpperRight;
                            uvs[topLeft] = uv.UpperLeft;
                        }

                        // North face
                        Chunk north = GetAdjacent(Direction.North);
                        if (x == 0 && north != null && north.Blocks[width - 1, z, y].BlockType == BlockType.Air || x != 0 && blocks[x - 1, z, y].BlockType == BlockType.Air)
                        {
                            var face = AddFaceVertices(x, y, z, vertices, Direction.North);
                            (int bottomLeft, int bottomRight, int topRight, int topLeft) = face;
                            BuildQuad(triangles, face);
                            BlockFace uv = blockFaces[(int) Direction.North];
                            uvs[bottomLeft] = uv.LowerLeft;
                            uvs[bottomRight] = uv.LowerRight;
                            uvs[topRight] = uv.UpperRight;
                            uvs[topLeft] = uv.UpperLeft;
                        }

                        // East face
                        Chunk east = GetAdjacent(Direction.East);
                        if (z == width - 1 && east != null && east.Blocks[x, 0, y].BlockType == BlockType.Air || z != width - 1 && blocks[x, z + 1, y].BlockType == BlockType.Air)
                        {
                            var face = AddFaceVertices(x, y, z, vertices, Direction.East);
                            (int bottomLeft, int bottomRight, int topRight, int topLeft) = face;
                            BuildQuad(triangles, face);
                            BlockFace uv = blockFaces[(int) Direction.East];
                            uvs[bottomLeft] = uv.LowerLeft;
                            uvs[bottomRight] = uv.LowerRight;
                            uvs[topRight] = uv.UpperRight;
                            uvs[topLeft] = uv.UpperLeft;
                        }

                        // South face
                        Chunk south = GetAdjacent(Direction.South);
                        if (x == width - 1 && south != null && south.Blocks[0, z, y].BlockType == BlockType.Air || x != width - 1 && blocks[x + 1, z, y].BlockType == BlockType.Air)
                        {
                            var face = AddFaceVertices(x, y, z, vertices, Direction.South);
                            (int bottomLeft, int bottomRight, int topRight, int topLeft) = face;
                            BuildQuad(triangles, face);
                            BlockFace uv = blockFaces[(int) Direction.South];
                            uvs[bottomLeft] = uv.LowerLeft;
                            uvs[bottomRight] = uv.LowerRight;
                            uvs[topRight] = uv.UpperRight;
                            uvs[topLeft] = uv.UpperLeft;
                        }

                        // West face
                        Chunk west = GetAdjacent(Direction.West);
                        if (z == 0 && west != null && west.Blocks[x, width - 1, y].BlockType == BlockType.Air || z != 0 && blocks[x, z - 1, y].BlockType == BlockType.Air)
                        {
                            var face = AddFaceVertices(x, y, z, vertices, Direction.West);
                            (int bottomLeft, int bottomRight, int topRight, int topLeft) = face;
                            BuildQuad(triangles, face);
                            BlockFace uv = blockFaces[(int) Direction.West];
                            uvs[bottomLeft] = uv.LowerLeft;
                            uvs[bottomRight] = uv.LowerRight;
                            uvs[topRight] = uv.UpperRight;
                            uvs[topLeft] = uv.UpperLeft;
                        }

                        // Top face
                        if (y == height - 1 || blocks[x, z, y + 1].BlockType == BlockType.Air)
                        {
                            var face = AddFaceVertices(x, y, z, vertices, Direction.Top);
                            (int topLeft, int topRight, int bottomRight, int bottomLeft) = face;
                            BuildQuad(triangles, face);

                            BlockFace uv = blockFaces[(int) Direction.Top];
                            uvs[bottomLeft] = uv.LowerLeft;
                            uvs[bottomRight] = uv.LowerRight;
                            uvs[topRight] = uv.UpperRight;
                            uvs[topLeft] = uv.UpperLeft;
                        }
                    }
                }
            }

            Vector2[] finalUv = new Vector2[vertices.Count];
            Array.Copy(uvs, finalUv, vertices.Count);
            Mesh mesh = new Mesh {indexFormat = IndexFormat.UInt32, vertices = vertices.ToArray(), uv = finalUv, triangles = triangles.ToArray()};
            mesh.Optimize();
            return mesh;
        }

        private (int, int, int, int) AddFaceVertices(int x, int y, int z, List<Vector3> vertices, Direction direction)
        {
            int index = (byte) direction * 4;
            int i1 = vertices.Count;
            vertices.Add(GetVertex(x, y, z, index));
            int i2 = vertices.Count;
            vertices.Add(GetVertex(x, y, z, index + 1));
            int i3 = vertices.Count;
            vertices.Add(GetVertex(x, y, z, index + 2));
            int i4 = vertices.Count;
            vertices.Add(GetVertex(x, y, z, index + 3));

            return (i1, i2, i3, i4);
        }

        private void BuildQuad(List<int> triangles, (int, int, int, int) vertices)
        {
            (int bottomLeft, int bottomRight, int topRight, int topLeft) = vertices;
            
            triangles.Add(bottomLeft);
            triangles.Add(topLeft);
            triangles.Add(topRight);
            
            triangles.Add(bottomLeft);
            triangles.Add(topRight);
            triangles.Add(bottomRight);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Vector3 GetVertex(int x, int y, int z, int index)
        {
            switch (index)
            {
                case 0:
                case 5:
                case 19:
                    return new Vector3(x, y, z);
                case 1:
                case 12:
                case 16:
                    return new Vector3(x + 1, y, z);
                case 2:
                case 15:
                case 20:
                    return new Vector3(x + 1, y + 1, z);
                case 3:
                case 6:
                case 23:
                    return new Vector3(x, y + 1, z);
                case 7:
                case 10:
                case 22:
                    return new Vector3(x, y + 1, z + 1);
                case 11:
                case 14:
                case 21:
                    return new Vector3(x + 1, y + 1, z + 1);
                case 8:
                case 13:
                case 17:
                    return new Vector3(x + 1, y, z + 1);
                case 4:
                case 9:
                case 18:
                    return new Vector3(x, y, z + 1);
                default:
                    throw new ArgumentOutOfRangeException(nameof(index), "Invalid vertex index");
            }
        }
    }
}
