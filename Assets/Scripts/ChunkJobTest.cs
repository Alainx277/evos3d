﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Rendering;
using Random = UnityEngine.Random;

public class ChunkJobTest : MonoBehaviour
{
    public int Width = 5;
    public int Height = 5;
    public Material BlockMaterial;
    
    [Range(0.0001f, 1)]
    public float Step;

    private float _perlinX, _perlinY;
    private BlockAtlas _atlas = new BlockAtlas();
    
    // Start is called before the first frame update
    void Start()
    {
        _perlinX = Random.Range(0f, 1f);
        _perlinY = Random.Range(0f, 1f);
        int blocksPerChunk = Chunk.Width * Chunk.Width * Chunk.Height;
        NativeArray<Vertex> vertices = new NativeArray<Vertex>(blocksPerChunk * 6 * 4, Allocator.Persistent);
        NativeArray<int> triangles = new NativeArray<int>(blocksPerChunk * 6 * 3 * 2, Allocator.Persistent);
        NativeArray<BlockData> blocks = new NativeArray<BlockData>(blocksPerChunk, Allocator.Persistent);
        NativeArray<int> outVertices = new NativeArray<int>(1, Allocator.Persistent);
        NativeArray<int> outTriangles = new NativeArray<int>(1, Allocator.Persistent);

        BlockType[] blockTypes = Enum.GetValues(typeof(BlockType)).Cast<BlockType>().Distinct().ToArray();
        NativeArray<BlockUv> uvs = new NativeArray<BlockUv>(blockTypes.Length, Allocator.Persistent);
        
        for (var i = 0; i < blockTypes.Length; i++)
        {
            BlockType type = blockTypes[i];
            BlockFace[] faces = _atlas.GetBlockFaces(type);
            BlockUv blockUv = new BlockUv
            {
                West = faces[0],
                North = faces[1],
                East = faces[2],
                South = faces[3],
                Bottom = faces[4],
                Top = faces[5]
            };
            uvs[i] = blockUv;
        }
        

        for (int x = 0; x < Width; x++)
        {
            for (int z = 0; z < Height; z++)
            {
                Chunk data = Chunk.Generate(_perlinX + Step * (x * Chunk.Width), _perlinY + Step * (z * Chunk.Width), Step, Step);
                
                // Map block data
                for (int blocksX = 0; blocksX < data.Blocks.GetLength(0); blocksX++)
                {
                    for (int blocksZ = 0; blocksZ < data.Blocks.GetLength(1); blocksZ++)
                    {
                        for (int blocksY = 0; blocksY < data.Blocks.GetLength(2); blocksY++)
                        {
                            blocks[blocksX + Chunk.Width * (blocksY + Chunk.Height * blocksZ)] =
                                data.Blocks[blocksX, blocksZ, blocksY];
                        }
                    }
                }
                
                var job = new ChunkMeshBuildJob
                {
                    Blocks = blocks,
                    Vertices = vertices,
                    Triangles = triangles,
                    ChunkHeight = Chunk.Height,
                    ChunkWidth = Chunk.Width,
                    OutTrianglesLength = outTriangles,
                    OutVerticesLength = outVertices,
                    UvMap = uvs
                };
                JobHandle handle = job.Schedule();
                handle.Complete();

                int numVertices = outVertices[0];
                int numTriangles = outTriangles[0];
                
                Mesh mesh = new Mesh();
                mesh.SetVertexBufferParams(numVertices, Vertex.GetVertexAttributes());
                mesh.SetVertexBufferData(job.Vertices, 0, 0, numVertices);
                mesh.SetIndexBufferParams(numTriangles, IndexFormat.UInt32);
                mesh.SetIndexBufferData(job.Triangles, 0, 0, numTriangles);
                mesh.subMeshCount = 1;
                mesh.SetSubMesh(0, new SubMeshDescriptor(0, numTriangles));
                mesh.RecalculateBounds();
                mesh.RecalculateNormals();
                
                CreateChunkObject(mesh, x, z);
            }
        }

        vertices.Dispose();
        triangles.Dispose();
        blocks.Dispose();
        outVertices.Dispose();
        outTriangles.Dispose();
    }

    private GameObject CreateChunkObject(Mesh mesh, int x, int z)
    {
        GameObject o = new GameObject();
        o.transform.parent = transform;
        o.transform.position = new Vector3(x * Chunk.Width, 0f, z * Chunk.Width);
        MeshRenderer meshRenderer = o.AddComponent<MeshRenderer>();
        meshRenderer.material = BlockMaterial;
        MeshFilter filter = o.AddComponent<MeshFilter>();
        filter.mesh = mesh;
        o.AddComponent<MeshCollider>().sharedMesh = mesh;
        return o;
    }
}
