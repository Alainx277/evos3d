﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public struct BlockFace
    {
        public Vector2 UpperLeft;
        public Vector2 UpperRight;
        public Vector2 LowerLeft;
        public Vector2 LowerRight;

        public BlockFace(Vector2 upperLeft, Vector2 upperRight, Vector2 lowerLeft, Vector2 lowerRight)
        {
            UpperLeft = upperLeft;
            UpperRight = upperRight;
            LowerLeft = lowerLeft;
            LowerRight = lowerRight;
        }
    }

    class BlockAtlas
    {
        private const int Width = 28, Height = 17;

        private Dictionary<BlockType, BlockFace[]> _cache = new Dictionary<BlockType, BlockFace[]>();

        /// <summary>
        /// Returns the 6 block faces for a block type
        /// </summary>
        /// <param name="type">The block type</param>
        /// <returns>A array with the following order: West, North, East, South, Bottom, Top</returns>
        public BlockFace[] GetBlockFaces(BlockType type)
        {
            if (_cache.ContainsKey(type))
            {
                return _cache[type];
            }
            
            var uvs = new BlockFace[6];

            BlockFace side;
            BlockFace top;
            BlockFace bottom;
            BlockFace face;
            switch (type)
            {
                case BlockType.Dirt:
                    face = FaceAtIndex(459);
                    for (var i = 0; i < uvs.Length; i++)
                    {
                        uvs[i] = face;
                    }
                    break;
                case BlockType.Stone:
                    face = FaceAtIndex(219);
                    for (var i = 0; i < uvs.Length; i++)
                    {
                        uvs[i] = face;
                    }
                    break;
                case BlockType.Grass:
                    side = FaceAtIndex(178);
                    top = FaceAtIndex(181);
                    bottom = FaceAtIndex(459);
                    for (int i = 0; i < 4; i++)
                    {
                        uvs[i] = side;
                    }

                    uvs[4] = bottom;
                    uvs[5] = top;
                    break;
                case BlockType.Snowy:
                    side = FaceAtIndex(180);
                    top = FaceAtIndex(206);
                    bottom = FaceAtIndex(459);
                    for (int i = 0; i < 4; i++)
                    {
                        uvs[i] = side;
                    }

                    uvs[4] = bottom;
                    uvs[5] = top;
                    break;
            }

            _cache.Add(type, uvs);
            return uvs;
        }

        private BlockFace FaceAtIndex(int index)
        {
            float edge = 0.0001f;
            float xOffset = 1f / Width;
            float yOffset = 1f / Height;
            float y = Mathf.Floor( index / (float)Width);
            float x = index % Width;
            float baseX = x / Width;
            float baseY = y / Height;
            
            return new BlockFace(new Vector2(baseX + edge, baseY + yOffset - edge),
                new Vector2(baseX + xOffset - edge, baseY + yOffset - edge),
                new Vector2(baseX + edge, baseY + edge),
                new Vector2(baseX + xOffset - edge, baseY + edge)
                );
        }
    }
}
