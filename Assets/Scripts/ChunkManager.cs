﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.U2D;
using Random = UnityEngine.Random;

struct ChunkObject
{
    public Chunk Chunk;
    public GameObject GameObject;

    public ChunkObject([NotNull] Chunk chunk, [NotNull] GameObject gameObject)
    {
        Chunk = chunk ?? throw new ArgumentNullException(nameof(chunk));
        GameObject = gameObject ? gameObject : throw new ArgumentNullException(nameof(gameObject));
    }
}

public class ChunkManager : MonoBehaviour
{

    public int ViewDistance = 10;
    public Material BlockMaterial;
    public Transform TargetTransform;
    public Sprite Atlas;


    [Range(0.0001f, 1)]
    public float Step;

    private float perlinX, perlinY;
    private Dictionary<ChunkIndex, ChunkObject> _chunks;
    private float lastChunkCheck = 0f;
    private BlockAtlas _atlas;

    // Start is called before the first frame update
    void Start()
    {
        _chunks = new Dictionary<ChunkIndex, ChunkObject>();
        perlinX = Random.Range(0f, 1f);
        perlinY = Random.Range(0f, 1f);
        _atlas = new BlockAtlas();
    }

    // Update is called once per frame
    void Update()
    {
        // Timer
        if (Time.time < lastChunkCheck + 0.5f)
        {
            return;
        }
        lastChunkCheck = Time.time;

        // Remove chunks far away
        foreach (var pair in _chunks.Where(pair =>
        {
            GameObject chunk = pair.Value.GameObject;
            ChunkIndex relativeIndex = pair.Key;
            ChunkIndex targetIndex = Chunk.WorldToChunkIndex(TargetTransform.position);
            relativeIndex.X = Mathf.Abs(relativeIndex.X - targetIndex.X);
            relativeIndex.Z = Mathf.Abs(relativeIndex.Z - targetIndex.Z);
            if (relativeIndex.X > ViewDistance || relativeIndex.Z > ViewDistance)
            {
                Destroy(chunk);
                return true;
            }

            return false;
        }))
        {
            _chunks.Remove(pair.Key);
        }

        // Generate new chunks
        ChunkIndex chunkIndex = Chunk.WorldToChunkIndex(TargetTransform.position);
        Dictionary<ChunkIndex, Chunk> newChunks = new Dictionary<ChunkIndex, Chunk>();
        for (int x = Mathf.Max(chunkIndex.X - ViewDistance, 0); x < chunkIndex.X + ViewDistance; x++)
        {
            for (int z = Mathf.Max(chunkIndex.Z - ViewDistance, 0); z < chunkIndex.Z + ViewDistance; z++)
            {
                if (!_chunks.Any(o => 
                    o.Key.Equals(new ChunkIndex(x, z))
                    ))
                {
                    newChunks.Add(new ChunkIndex(x, z), Chunk.Generate(perlinX + Step * (x * Chunk.Width), perlinY + Step * (z * Chunk.Width), Step, Step));
                }
            }
        }

        ChunkObject[] toRebuild = _chunks.Where(p => newChunks.Any(pair => IsAdjacent(p.Key, pair.Key))).Select(x => x.Value).ToArray();

        var addedChunks = newChunks.ToArray();
        var existingChunks = _chunks.Select(x => new KeyValuePair<ChunkIndex, Chunk>(x.Key, x.Value.Chunk)).ToArray();

        // Build new chunk meshes
        foreach (var pair in newChunks)
        {
            var pos = pair.Key;
            ChunkMeshBuilder meshBuilder = new ChunkMeshBuilder(pair.Value, _atlas);
            AddAdjacentChunks(meshBuilder, pos, addedChunks);
            AddAdjacentChunks(meshBuilder, pos, existingChunks);
            
            _chunks.Add(pos, new ChunkObject(pair.Value, CreateChunkObject(meshBuilder, pos.X, pos.Z)));
            Debug.Log("Built new chunk");
        }
        
        // Rebuild existing meshes
        foreach (ChunkObject chunkObject in toRebuild)
        {
            ChunkMeshBuilder meshBuilder = new ChunkMeshBuilder(chunkObject.Chunk, _atlas);
            ChunkIndex index = Chunk.WorldToChunkIndex(chunkObject.GameObject.transform.position);
            AddAdjacentChunks(meshBuilder, index, existingChunks);
            AddAdjacentChunks(meshBuilder, index, addedChunks);
            Mesh mesh = meshBuilder.Build();
            chunkObject.GameObject.GetComponent<MeshFilter>().mesh = mesh;
            chunkObject.GameObject.GetComponent<MeshCollider>().sharedMesh = mesh;
            Debug.Log("Rebuilt chunk");
        }
    }

    private bool IsAdjacent(ChunkIndex position, ChunkIndex other)
    {
        if (position.X == other.X)
        {
            return position.Z + 1 == other.Z || position.Z - 1 == other.Z;
        }

        if (position.Z == other.Z)
        {
            return position.X + 1 == other.X || position.X - 1 == other.X;
        }

        return false;
    }
    
    private void AddAdjacentChunks(ChunkMeshBuilder meshBuilder, ChunkIndex position, KeyValuePair<ChunkIndex, Chunk>[] chunks)
    {
        var pairs = chunks;
        // North
        Chunk north = pairs.FirstOrNull(p => p.Key == new ChunkIndex(position.X - 1, position.Z))?.Value;
        if (north != null)
        {
            meshBuilder.AddAdjacent(Direction.North, north);
        }
        
        // East
        Chunk east = pairs.FirstOrNull(p => p.Key == new ChunkIndex(position.X, position.Z + 1))?.Value;
        if (east != null)
        {
            meshBuilder.AddAdjacent(Direction.East, east);
        }
        // South
        Chunk south = pairs.FirstOrNull(p => p.Key == new ChunkIndex(position.X + 1, position.Z))?.Value;
        if (south != null)
        {
            meshBuilder.AddAdjacent(Direction.South, south);
        }
        // West
        Chunk west = pairs.FirstOrNull(p => p.Key == new ChunkIndex(position.X, position.Z - 1))?.Value;
        if (west != null)
        {
            meshBuilder.AddAdjacent(Direction.West, west);
        }
    }

    private GameObject CreateChunkObject(ChunkMeshBuilder meshBuilder, int x, int z)
    {
        GameObject o = new GameObject();
        o.transform.parent = transform;
        o.transform.position = new Vector3(x * Chunk.Width, 0f, z * Chunk.Width);
        MeshRenderer meshRenderer = o.AddComponent<MeshRenderer>();
        meshRenderer.material = BlockMaterial;
        MeshFilter filter = o.AddComponent<MeshFilter>();
        filter.mesh = meshBuilder.Build();
        o.AddComponent<MeshCollider>().sharedMesh = filter.mesh;
        return o;
    }
}
