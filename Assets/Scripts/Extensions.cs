﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts
{
    public static class Extensions
    {
        public static T? FirstOrNull<T>(this IEnumerable<T> source, Func<T, bool> predicate) where T : struct
        {
            foreach (T val in source)
            {
                if (predicate(val))
                {
                    return val;
                }
            }

            return null;
        }
    }
}