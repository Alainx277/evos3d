﻿using System.Runtime.InteropServices;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Rendering;

namespace Assets.Scripts
{
    [StructLayout(LayoutKind.Sequential)]
    struct FaceVertices
    {
        public int BottomLeft;
        public int BottomRight;
        public int TopLeft;
        public int TopRight;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Vertex
    {
        public float3 Position;
        public float2 Normal;

        public static VertexAttributeDescriptor[] GetVertexAttributes()
        {
            return new[]
            {
                new VertexAttributeDescriptor(VertexAttribute.Position),
                new VertexAttributeDescriptor(VertexAttribute.Normal, VertexAttributeFormat.Float32, 2), 
            };
        }
    }
    
    
    [StructLayout(LayoutKind.Sequential)]
    public struct BlockUv
    {
        public BlockFace Top;
        public BlockFace Bottom;
        public BlockFace North;
        public BlockFace East;
        public BlockFace South;
        public BlockFace West;
    }
    
    [BurstCompile]
    public struct ChunkMeshBuildJob : IJob
    {
        [ReadOnly] 
        public NativeArray<BlockData> Blocks;

        [ReadOnly] 
        public NativeArray<BlockUv> UvMap;
        /*[ReadOnly] 
        public NativeArray<BlockData> NorthAdjacent;
        public bool HasNorth;
        [ReadOnly] 
        public NativeArray<BlockData> EastAdjacent;
        public bool HasEast;
        [ReadOnly] 
        public NativeArray<BlockData> SouthAdjacent;
        public bool HasSouth;
        [ReadOnly] 
        public NativeArray<BlockData> WestAdjacent;
        public bool HasWest;*/
        public NativeArray<Vertex> Vertices;
        [WriteOnly]
        public NativeArray<int> Triangles;

        public int ChunkHeight;
        public int ChunkWidth;

        [WriteOnly]
        public NativeArray<int> OutVerticesLength;
        [WriteOnly]
        public NativeArray<int>  OutTrianglesLength;

        private int _verticesLength;
        private int _trianglesLength;
        private const int AtlasWidth = 28;
        private const int AtlasHeight = 17;
        
        public void Execute()
        {
            for (int x = 0; x < ChunkWidth; x++)
            {
                for (int z = 0; z < ChunkWidth; z++)
                {
                    for (int y = 0; y < ChunkHeight; y++)
                    {
                        BlockData block = GetBlockData(x, z, y);
                        if (block.BlockType == BlockType.Air)
                        {
                            continue;
                        }

                        BlockUv uv = UvMap[(int) block.BlockType];

                        if (y == ChunkHeight - 1 || GetBlockData(x, z, y + 1).BlockType == BlockType.Air)
                        {
                            var face = AddFaceVertices(x, y, z, Direction.Top);
                            BuildQuad(face);
                            ApplyUv(face, uv.Top);
                        }
                        
                        if (y != 0 && GetBlockData(x, z, y - 1).BlockType == BlockType.Air)
                        {
                            var face = AddFaceVertices(x, y, z, Direction.Bottom);
                            BuildQuad(face);
                            ApplyUv(face, uv.Bottom);
                        }

                        if (x == 0 || GetBlockData(x - 1, z, y).BlockType == BlockType.Air)
                        {
                            var face = AddFaceVertices(x, y, z, Direction.North);
                            BuildQuad(face);
                            ApplyUv(face, uv.North);
                        }
                        
                        if (z == ChunkWidth - 1 || GetBlockData(x , z + 1, y).BlockType == BlockType.Air)
                        {
                            var face = AddFaceVertices(x, y, z, Direction.East);
                            BuildQuad(face);
                            ApplyUv(face, uv.East);
                        }
                        
                        if (x == ChunkWidth - 1 || GetBlockData(x + 1, z, y).BlockType == BlockType.Air)
                        {
                            var face = AddFaceVertices(x, y, z, Direction.South);
                            BuildQuad(face);
                            ApplyUv(face, uv.South);
                        }
                        
                        if (z == 0 || GetBlockData(x, z - 1, y).BlockType == BlockType.Air)
                        {
                            var face = AddFaceVertices(x, y, z, Direction.West);
                            BuildQuad(face);
                            ApplyUv(face, uv.West);
                        }
                    }
                }
            }

            OutVerticesLength[0] = _verticesLength;
            OutTrianglesLength[0] = _trianglesLength;
        }
        
        private void BuildQuad(FaceVertices vertices)
        {
            AddTriangle(vertices.BottomLeft);
            AddTriangle(vertices.TopLeft);
            AddTriangle(vertices.TopRight);

            AddTriangle(vertices.BottomLeft);
            AddTriangle(vertices.TopRight);
            AddTriangle(vertices.BottomRight);
        }

        private FaceVertices AddFaceVertices(int x, int y, int z, Direction direction)
        {
            int index = (byte) direction * 4;
            int i1 = _verticesLength;
            Vertices[_verticesLength] = new Vertex {Position = GetVertex(x, y, z, index)};
            _verticesLength++;
            int i2 = _verticesLength;
            Vertices[_verticesLength] = new Vertex {Position = GetVertex(x, y, z, index + 1)};
            _verticesLength++;
            int i3 = _verticesLength;
            Vertices[_verticesLength] = new Vertex {Position = GetVertex(x, y, z, index + 2)};
            _verticesLength++;
            int i4 = _verticesLength;
            Vertices[_verticesLength] = new Vertex {Position = GetVertex(x, y, z, index + 3)};
            _verticesLength++;

            return new FaceVertices()
            {
                BottomLeft = i1,
                BottomRight = i2,
                TopRight = i3,
                TopLeft = i4
            };
        }

        private void ApplyUv(FaceVertices vertices, BlockFace uv)
        {
            Vertices[vertices.BottomLeft] = new Vertex
                {Position = Vertices[vertices.BottomLeft].Position, Normal = uv.LowerLeft};
            
            Vertices[vertices.BottomRight] = new Vertex
                {Position = Vertices[vertices.BottomRight].Position, Normal = uv.LowerRight};
            
            Vertices[vertices.TopLeft] = new Vertex
                {Position = Vertices[vertices.TopLeft].Position, Normal = uv.UpperLeft};
            
            Vertices[vertices.TopRight] = new Vertex
                {Position = Vertices[vertices.TopRight].Position, Normal = uv.UpperRight};
        }

        private BlockData GetBlockData(int x, int z, int y)
        {
            return Blocks[x + ChunkWidth * (y + ChunkHeight * z)];
        }
        
        private void AddTriangle(int tri)
        {
            Triangles[_trianglesLength] = tri;
            _trianglesLength++;
        }
        
        private float3 GetVertex(int x, int y, int z, int index)
        {
            switch (index)
            {
                case 0:
                case 5:
                case 19:
                    return new float3(x, y, z);
                case 1:
                case 12:
                case 16:
                    return new float3(x + 1, y, z);
                case 2:
                case 15:
                case 20:
                    return new float3(x + 1, y + 1, z);
                case 3:
                case 6:
                case 23:
                    return new float3(x, y + 1, z);
                case 7:
                case 10:
                case 22:
                    return new float3(x, y + 1, z + 1);
                case 11:
                case 14:
                case 21:
                    return new float3(x + 1, y + 1, z + 1);
                case 8:
                case 13:
                case 17:
                    return new float3(x + 1, y, z + 1);
                case 4:
                case 9:
                case 18:
                    return new float3(x, y, z + 1);
                default:
                    return new float3(x, y, z);
            }
        }
        
         /// <summary>
        /// Returns the 6 block faces for a block type
        /// </summary>
        /// <param name="type">The block type</param>
        /// <param name="uvs">The array the faces will be put into (length 6)</param>
        /// <returns>A array with the following order: West, North, East, South, Bottom, Top</returns>
        private void GetBlockFaces(BlockType type, BlockFace[] uvs)
        {
            BlockFace side;
            BlockFace top;
            BlockFace bottom;
            BlockFace face;
            switch (type)
            {
                case BlockType.Dirt:
                    face = FaceAtIndex(459);
                    for (var i = 0; i < uvs.Length; i++)
                    {
                        uvs[i] = face;
                    }
                    break;
                case BlockType.Stone:
                    face = FaceAtIndex(219);
                    for (var i = 0; i < uvs.Length; i++)
                    {
                        uvs[i] = face;
                    }
                    break;
                case BlockType.Grass:
                    side = FaceAtIndex(178);
                    top = FaceAtIndex(181);
                    bottom = FaceAtIndex(459);
                    for (int i = 0; i < 4; i++)
                    {
                        uvs[i] = side;
                    }

                    uvs[4] = bottom;
                    uvs[5] = top;
                    break;
                case BlockType.Snowy:
                    side = FaceAtIndex(180);
                    top = FaceAtIndex(206);
                    bottom = FaceAtIndex(459);
                    for (int i = 0; i < 4; i++)
                    {
                        uvs[i] = side;
                    }

                    uvs[4] = bottom;
                    uvs[5] = top;
                    break;
            }
        }

        private BlockFace FaceAtIndex(int index)
        {
            float edge = 0.0001f;
            float xOffset = 1f / AtlasWidth;
            float yOffset = 1f / AtlasHeight;
            float y = Mathf.Floor( index / (float)AtlasWidth);
            float x = index % AtlasWidth;
            float baseX = x / AtlasWidth;
            float baseY = y / AtlasHeight;
            
            return new BlockFace(new Vector2(baseX + edge, baseY + yOffset - edge),
                new Vector2(baseX + xOffset - edge, baseY + yOffset - edge),
                new Vector2(baseX + edge, baseY + edge),
                new Vector2(baseX + xOffset - edge, baseY + edge)
                );
        }
    }
}